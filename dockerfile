#Getting the latest python image
FROM python:3.11

#labels as key value pair
LABEL Maintainer="@Gogden"

ENV TERM=xterm-256color

#Working Directory
WORKDIR /usr/app/src

#Copy source files
COPY requirements.txt src\*.py ./
RUN pip install --upgrade pip
RUN apt-get update && apt-get install -y gcc pkg-config
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "-u", "./main.py", "--debug", "--term" ]