import os
from custom_print import *
import translate as t
from dotenv import load_dotenv
from bot import init_bot
import argparse

def main(args):
    if args.debug:
        set_loglevel(DEBUG)
    print_header()
    dbg_print("Downloading and Installing language models...")
    t.install_argos_translate()
    dbg_print("Language models installed!")
    dbg_print("Processing Discord information...")
    TOKEN = str(args.term[1])
    CLIENT_ID = int(args.term[0])
    info_print(f"Discord information retrieved:\n\tDiscord Token:\t\t{TOKEN}\n\tDiscord Client ID:\t{CLIENT_ID}")
    dbg_print("Setting up discord client...")
    client = init_bot()
    dbg_print("Discord client set up. Connecting to discord...")
    info_print(f"Invite bot with the following link:\n\thttps://discord.com/oauth2/authorize?client_id={CLIENT_ID}&scope=bot&permissions=377957125120")
    client.run(TOKEN)

    


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="A discord bot that provides free translations for your server!")
    subparsers = parser.add_subparsers()
    envOrTerm = parser.add_mutually_exclusive_group(required=True)
    envOrTerm.add_argument("--env",help="The ID and Token are provided as environment variables.", action="store_true", required=False)
    envOrTerm.add_argument("--term",help="The Client ID and then token provided by Discord Developer Portal.", nargs=2, metavar=("clientID","token"))
    parser.add_argument("--debug",help="Enable Debug Logging.", action="store_true" , required=False)
    args = parser.parse_args()
    main(args)