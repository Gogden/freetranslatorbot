from termcolor import colored

"""Available text colors:
    black, red, green, yellow, blue, magenta, cyan, white,
    light_grey, dark_grey, light_red, light_green, light_yellow, light_blue,
    light_magenta, light_cyan."""
debug_color = "blue"
info_color = "green"
error_color = "red"
DEBUG = 50
INFO = 30
ERROR = 10
_loglevel = INFO


def set_loglevel(level:int):
    global _loglevel
    _loglevel = level

def get_loglevel()->int:
    global _loglevel
    return _loglevel

def configure_custom_print(debug:str="blue",info:str="green",error:str="red"):
    global debug_color
    global info_color
    global error_color
    debug_color = debug
    info_color = info
    error_color = error


def dbg_print(text:str):
    global DEBUG
    if _loglevel >= DEBUG:
        _color_print("[DEBUG]"+text,debug_color)

def info_print(text:str):
    global INFO
    if _loglevel >= INFO:
        _color_print("[INFO]"+text,info_color)

def err_print(text:str):
    global ERROR
    if _loglevel >= ERROR:
        _color_print("[ERROR]"+text,error_color)

def _color_print(text:str, color:str):
    print(colored(text, color))

def print_header():
    _color_print("  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAE                               \n"+
"  IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACW                               \n"+
"  IAAAAAAAR YYYYYY FAAAAAAAAAAV YYYYY  AAAAAAACW                               \n"+
"  IAAAAAAC          BAAAAAAAAQ         RAAAAAACW                               \n"+
"  IAAAAL               AAAA               AAAAAW                               \n"+
"  AAA                 PAAAAV                IAAI                               \n"+
"  WEW                 PAAAAV                 J                                 \n"+
"                      PAAAAV                                                   \n"+
"                      PAAAAV                   AAA         JAAAAAAA            \n"+
"                      PAAAAV         AAA    OAAAAI    IAAAAAAAAAAAAAAG         \n"+
"                      PAAAAV       OGAAR  VMAAAAAS  SKAAAAAAYMSOAAAAAZ         \n"+
"                      PAAAAV      RAAAA   AAAAAAA   AAAAAAAU    BAAAA          \n"+
"                      PAAAAV   MAAAAAAAAAAAAAAAAAAAAAAAAA          AAAAAV      \n"+
"                      PAAAAV    AAB  MAAAF   AAAAAAAAAR           FAAAA        \n"+
"                      PAAAAV JAADCZ  EAAAA  SAAAAD PIG            XMMBAHHSV    \n"+
"                      PAAAAV AAAM    AAAAA  IAAAA                    TAAAAB    \n"+
"                      PAAAAV         AAAAA  IAAAAI                   EAAAAQ    \n"+
"                      PAAAAV         AAAAA  IAAAAI    IAAR           EAAAAQ    \n"+
"                      WAAAA          AAAAA  IAAAAI  MJAAA            KAAAAK    \n"+
"                       AAAA          AAAAA  IAAAAI SAAAAX            VAAAAB    \n"+
"                    AAAAAAAAAH       AAAAA  IAAAAI NAAAAAAAT       AAAA        \n"+
"                 MAAAAAAAAAAAAAB     AAAAA  IAAAAI    AAAAAAAAAAAAAAAAAA       \n"+
"              YLFAAAAAAAAAAAAAAAAMO  AAAAA  IAAAAI    UGAAAAAAAAAAAAFBAO       \n"+
"              AAAAAAAAAAAAAAAAAAAAA  AAAAA  IAAAAI       AAAAAAAAAA            \n"+
"                                     AAAAA  IAAAAI                             \n"+
"                                     AAAAA  IAAAAI                             \n"+
"                                     AAAAA  IAAAAI                             \n"+
"                                     AAAAA  IAAAAI                             \n"+
"                                     AAAAA  IAAAAI                             \n"+
"                                     AAAAA  XAAAAA                             \n"+
"                                     AAAAA  KAAAAF                             \n"+
"                                     AAAAAAAAAA                                \n"+
"                                     AAAAAAAN                                  \n"+
"                                     HAAAAR                                    \n"+
"                                     AAAAE                                     \n"+
"                                  VAAAAQ                                       \n"+
"                               OAAAAA                                          \n"+
"                               RAAA                                            \n"+
"                               WBAQ                                            ","light_magenta")
    _color_print("  ______          _______                  _       _             ____        _   \n"+
" |  ____|        |__   __|                | |     | |           |  _ \      | |  \n"+
" | |__ _ __ ___  ___| |_ __ __ _ _ __  ___| | __ _| |_ ___  _ __| |_) | ___ | |_ \n"+
" |  __| '__/ _ \/ _ \ | '__/ _` | '_ \/ __| |/ _` | __/ _ \| '__|  _ < / _ \| __|\n"+
" | |  | | |  __/  __/ | | | (_| | | | \__ \ | (_| | || (_) | |  | |_) | (_) | |_ \n"+
" |_|  |_|  \___|\___|_|_|  \__,_|_| |_|___/_|\__,_|\__\___/|_|  |____/ \___/ \__|\n", "light_cyan")




                                                                                 
                                                                                 
